public class Lion extends Pet{

    public Lion() {
        this.setNumLegs(4);
    }

    public Lion(String name){
        this();
        this.setName(name);
    }

    public void feed(){
        System.out.println("feed Lion a sheep");
    }

    public void feed(String food){
        System.out.println("feed Lion " + food);
    }

    public void exclaim(){
        System.out.println("Rawr!!");
    }
}
